#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/ethernet.h>
#include <linux/in.h>
#include <csignal>

struct Radiotap_hdr {
        u_int8_t        header_version;
        u_int8_t        header_pad;
        u_int16_t       header_len;
        u_int32_t       present_flags_word1;
        u_int32_t       present_flags_word2;
        u_int8_t        flags;
        u_int8_t        data_rate;
        u_int16_t       chan_frequency;
        u_int16_t       chan_flags;
        u_int8_t        antenna_signal1;
        u_int16_t       rx_flags;
        u_int8_t        antenna_signal2;
        u_int8_t        antenna;
};

struct Beacon{
    u_int16_t type;
    u_int16_t frame_control_field;
    u_int8_t dst_addr[6];
    u_int8_t src_addr[6];
    u_int8_t BSSID[6];
    u_int16_t frag_seq_number;
};

struct Wireless{
    u_int8_t timestamp[8];
    u_int16_t beacon_interval;
    u_int16_t capabilties_info;
    u_int8_t tag_num;
    u_int8_t tag_len;
};

struct bss_list {
	u_int8_t bssid[6];
	u_int32_t beacons;
	u_int8_t essid[50];
	struct bss_list *next;
} __attribute__((packed));

struct bss_list* bss_first = NULL;

void usage(){
    printf("syntax: airodump <interface>\n");
    printf("sample: airodump mon0\n");
}

void signalHandler(int signal) {
	if(signal == SIGINT) {
		printf("\nQuitting...");
		exit(0);
	}
}

int main(int argc, char* argv[]){

    if (argc != 2) {
        usage();
        return -1;
    }

    struct Radiotap_hdr* r_hdr;
    struct Beacon* beacon;
    struct Wireless* wrls;

    char* dev = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE];

    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);

    if (handle == nullptr) {
        fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
        return -1;
    }

    signal(SIGINT, signalHandler);

    while(true) {
        struct pcap_pkthdr* header;
	struct bss_list* bss;

        const u_char* packet;
	const u_char* essid;

        int res = pcap_next_ex(handle, &header, &packet);
        if (res == 0) continue;
        if (res == -1 || res == -2) {
            printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
            break;
        }

        r_hdr = (struct Radiotap_hdr*)packet;
        beacon = (struct Beacon*)(packet+r_hdr->header_len);
        wrls = (struct Wireless*)(packet+r_hdr->header_len+sizeof(struct Beacon));
        packet = packet + r_hdr->header_len+sizeof(struct Beacon)+sizeof(struct Wireless);
	
	if(beacon->type != 0x0080) continue;
	bss = bss_first;
	essid = packet;
	
	u_int8_t bssid[6];
	for(int i=0;i<6;i++)
		bssid[i] = beacon->BSSID[i];

	while(bss != NULL) {
		if(memcmp(bss->bssid, bssid, 6) == 0) {
				bss->beacons++;
				break;
		}
		bss = bss->next;
	}
	if(bss == NULL) {
		bss = (struct bss_list*)malloc(sizeof(struct bss_list));
		memcpy(bss->bssid, bssid, 6);
		bss->beacons = 1;
		memcpy(bss->essid, essid, wrls->tag_len);
		bss->essid[wrls->tag_len] = '\0';
		bss->next = bss_first;
		bss_first = bss;

        }

	system("clear");
	printf("BSSID\t\t\tBeacons\tESSID\n");
	bss = bss_first;
	while(bss->next != NULL) {
		printf("%02x:%02x:%02x:%02x:%02x:%02x\t", bss->bssid[0], bss->bssid[1], bss->bssid[2], bss->bssid[3], bss->bssid[4], bss->bssid[5]);
		printf("%d\t%s\n", bss->beacons, bss->essid);
		bss = bss->next;
	}
    }

    pcap_close(handle);
}
