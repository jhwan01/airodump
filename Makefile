all: airodump

airodump: main.cpp
	gcc -o airodump main.cpp -lpcap

clean:
	rm -rf airodump *.o
